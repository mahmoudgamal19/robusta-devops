#!/bin/bash

# Wait for MySQL to be ready
echo "Waiting for MySQL..."
while ! mysqladmin ping -h"$DB_HOST" --silent; do
    echo 'waiting for mysql...'
    sleep 3
done

# Run migrations
php artisan migrate --force

# Run tests
vendor/bin/phpunit
