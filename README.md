# Laravel Kubernetes CI/CD Pipeline

This project demonstrates a CI/CD pipeline for deploying a Laravel application on Kubernetes using Helm. The pipeline automates the building, testing, and deployment of the application, ensuring a seamless and efficient workflow from development to production.

## Table of Contents
- [Assumptions and Prioritization](#assumptions-and-prioritization)
- [Architecture](#architecture)
- [CI/CD Pipeline](#cicd-pipeline)
- [Repository Contents](#repository-contents)
- [Prerequisites](#prerequisites)
- [Setup Instructions](#setup-instructions)
- [Deployment](#deployment)
- [Testing Process](#testing-process)
- [Troubleshooting](#troubleshooting)


## Assumptions and Prioritization

### Building the Laravel Application

I started by building a simple Laravel application with an endpoint `/test`. Using Test-Driven Development (TDD), I ensured that this endpoint was thoroughly tested. This approach helped in identifying and fixing issues early in the development cycle.

### Dockerizing the Application

Once the application was ready, I created a `Dockerfile` for the Laravel application. This `Dockerfile` defines the environment in which the application runs, ensuring consistency across different environments. To verify that everything worked correctly, I used Docker Compose to spin up the application along with its dependencies (MySQL and Redis) locally.

### Setting Up the CI/CD Pipeline

After confirming that the application worked locally, I moved on to setting up the CI/CD pipeline. The pipeline is configured to automatically build the Docker image of the Laravel application and push it to Docker Hub, but only after it passes all the unit tests. This ensures that only tested and verified code is deployed.

### Pushing to Docker Hub

With the CI/CD pipeline in place, the built Docker image is pushed to Docker Hub. This allows for easy deployment of the application to any environment, including the Kubernetes cluster used in this project.

### Assumptions

- The local development environment has Docker, Kubernetes, Helm, and GitLab CI/CD set up.
- The Kubernetes cluster is running and accessible.
![Kubernetes Cluster Status](https://gitlab.com/mahmoudgamal19/robusta-devops/-/raw/main/Screenshot_from_2024-06-02_12-40-32.png)
- Docker Hub is used for storing Docker images.

### Prioritization

- Focused on ensuring the core components (Laravel, MySQL, Redis) are containerized and deployed.
- Prioritized the setup of CI/CD pipeline to automate testing and deployment.

## Architecture
- **Kubernetes**: The orchestration platform for managing containerized applications.
  - Kubernetes manages the deployment, scaling, and operations of containerized applications across a cluster of nodes.

- **NFS Server**: I have used a NFS server as a persistent volume, its IP is included in the PV manifests, you can test it by changing  the IP to yours.

- **Laravel Application**: The main application running the Laravel framework.
  - The Laravel application is exposed using a **NodePort** service, making it accessible publicly. This service type allows external access to the application on a specific port of each node in the cluster.
  - Alternatively, the service could be configured as a **LoadBalancer** if a load balancer is available or by installing MetalLB on a bare metal cluster. However, for simplicity, a NodePort service is used in this setup.

- **MySQL**: The database service for storing application data.
  - MySQL is exposed using a **ClusterIP** service. This service type restricts accessibility to within the cluster, ensuring that the database is only accessible by other services within the Kubernetes cluster for enhanced security.

- **Redis**: The caching service used by Laravel.
  - Redis is also exposed using a **ClusterIP** service. Similar to MySQL, this restricts accessibility to within the cluster, ensuring that the caching service is only accessible internally.

- **Helm**: The package manager for Kubernetes used to deploy the application.
  - Helm is used to define, install, and upgrade the Kubernetes applications. The Helm chart in this repository encapsulates all the Kubernetes resources required to deploy the Laravel application, MySQL, and Redis.

I have set the Laravel app to have 2 replicas, one on each node and the redis and DB only have 1 one.

Below is a diagram of how the app is distributed inside the cluster

![Diagram For Cluster Components](https://diagramgenius.com/diagrams/images/3cdbc4d3-667e-4823-b663-1d98bf22c6dc.svg)

This architecture ensures that sensitive services like MySQL and Redis are only accessible internally, while the Laravel application is accessible publicly. The use of NodePort for the Laravel service provides a simple way to expose the application externally, while ClusterIP services for MySQL and Redis enhance security by limiting their exposure.


## CI/CD Pipeline

### 1. Developer Commits Code
- Developers make changes to the Laravel application and commit code to the Git repository.

### 2. CI Server Triggers Build and Test Jobs
- A CI server (GitLab CI/CD) triggers build and test jobs upon detecting a commit.

### 3. CI Server Builds Docker Images
- The CI server builds Docker images for the Laravel application, MySQL, and Redis.
- Dockerfiles for each service define how the images should be built.

### 4. Docker Images Pushed to Docker Registry
- The built Docker images are pushed to a Docker registry (e.g., Docker Hub), adding commit SHA as a tag for easy tracking the versions.
- **GitLab CI/CD Variables**: Ensure you have the following variables set in your GitLab repository settings:
  - `DOCKER_USERNAME`: Your Docker Hub username.
  - `DOCKER_PASSWORD`: Your Docker Hub password or access token.
  - `DOCKER_REGISTRY`: The Docker registry URL (e.g., `docker.io`).

### 5. CD Server Deploys to Kubernetes
- A CD server (ArgoCD) pulls the latest Docker images from the registry.
- The CD server uses Helm charts to deploy the Docker images to a Kubernetes cluster.

Here is a diagram for the CI/CD process

![CI/CD Pipeline Diagram](https://diagramgenius.com/diagrams/images/3df38587-381e-4531-9365-4ba774845a3a.svg)

## Repository Contents

### Helm Folder

The `helm` folder contains the Helm chart used to deploy the Laravel application on Kubernetes. It includes the following key components:

- **templates Folder**: This folder holds the Kubernetes manifests that define the Kubernetes resources for the application. The files include:
  - `deployments.yaml`: Defines the deployment resources for Laravel, MySQL, and Redis.
  - `services.yaml`: Defines the service resources to expose the deployments.
  - `secret.yaml`: Contains the secret resources for storing sensitive data.
  - `configmap.yaml`: Stores non-sensitive configuration data.
  - `pv.yaml` and `pvc.yaml`: Define the persistent volumes and persistent volume claims for data storage.
  
- **values.yaml**: This file contains the default configuration values for the Helm chart. It allows customization of the deployment by overriding these values during installation or upgrade.

- **Chart.yaml**: Provides metadata about the Helm chart, including its name, version, and description. This file is essential for Helm to identify and manage the chart.

### Laravel-App Folder

The `laravel-app` folder contains the Laravel project. It includes all the source code, configuration files, and resources needed to build and run the Laravel application. Key components include:

- `app`, `config`, `database`, `resources`, `routes`: Standard Laravel directories containing application logic, configuration settings, database migrations, views, and route definitions.
- `Dockerfile`: Defines the instructions to build the Docker image for the Laravel application, ensuring a consistent runtime environment.
- `.env`: Contains environment-specific configuration, such as database connection details and application settings.

### .gitlab-ci.yml

The `.gitlab-ci.yml` file defines the CI/CD pipeline configuration for GitLab. It specifies the jobs and stages to be executed, ensuring an automated workflow from code commit to deployment. Key aspects include:

- **Build Stage**: Builds the Docker image for the Laravel application.
- **Test Stage**: Runs unit tests to ensure the code functions correctly.
- **Deploy Stage**: Pushes the Docker image to Docker Hub and deploys the application to Kubernetes using Helm.

By leveraging GitLab CI/CD, Docker, and Kubernetes, this repository ensures a robust, automated, and scalable deployment pipeline for the Laravel application.


## Prerequisites

- Docker
- Kubernetes cluster
- Helm
- GitLab CI/CD
- Docker registry (Docker Hub)

## Setup Instructions

### Prerequisites

Ensure you have the following installed:
- Docker
- Docker Compose
- Kubernetes
- Helm

### Setup Docker Environment

1. **Clone the Repository**:
   ```bash
   git clone https://gitlab.com/mahmoudgamal19/robusta-devops.git
   cd robusta-devops
   ```

2. **Build Docker Images**:
    ```sh
    cd laravel-app
    docker-compose build
    ```

3. **Start Docker Containers**:
    ```sh
    docker-compose up -d
    ```

4. **Verify Docker Containers**:
    ```sh
    docker-compose ps
    ```

### Convert Docker Compose to Helm

1. **Install Helm**:
    Follow the installation instructions for Helm from the official [Helm documentation](https://helm.sh/docs/intro/install/).

2. **Create a Helm Chart**:
    Navigate to the directory where you want to create your Helm chart and run:
    ```sh
    helm create my-laravel-app
    ```

3. **Organize Your Helm Chart**:
    Copy your Docker Compose service definitions into the appropriate Helm templates. For example, in `helm/templates/`, create separate YAML files for your services (`mysql`, `redis`, `laravel`).

4. **Configure Values**:
    Update `helm/values.yaml` to define the necessary configuration values. 

5. **Deploy with Helm**:
    Deploy your Helm chart to your Kubernetes cluster with:
    ```sh
    helm install laravel ./helm
    ```

6. **Verify Deployment**:
    Ensure your services are running correctly:
    ```sh
    kubectl get pods
    kubectl get svc
    ```

### Accessing the Application

1. **Retrieve Service URL**:
    If you are using a NodePort service, get theport to be added to the IP:
    ```sh
    kubectl get svc laravel
    ```

2. **Access Application**:
    Open the retrieved IP address in your web browser to access the Laravel application.

### Managing Helm Releases

1. **Upgrade Release**:
    Make changes to your Helm chart and then upgrade:
    ```sh
    helm upgrade laravel ./helm
    ```

2. **Rollback Release**:
    If needed, rollback to a previous release:
    ```sh
    helm rollback laravel [REVISION]
    ```

3. **Uninstall Release**:
    To uninstall and clean up resources:
    ```sh
    helm uninstall laravel
    ```

### Additional Notes

- Customize `values.yaml` as per your application's requirements.
- Use secrets for sensitive information like database passwords.

By following these steps, you can set up your Laravel application with MySQL and Redis using Helm in a Kubernetes environment.

## Testing Process

### Summary

The testing process was carried out to ensure the robustness and reliability of the Laravel application, particularly the `/test` endpoint. The following steps were taken to implement and validate the tests:

1. **Unit Testing**:
   - Developed unit tests for the `/test` endpoint using Laravel's built-in testing framework.
   - Executed the tests locally to ensure the endpoint behaved as expected.

2. **Docker Compose Testing**:
   - Created a `docker-compose.yml` file to spin up the Laravel application along with MySQL and Redis.
   - Verified that the application and its dependencies worked correctly in a containerized environment.
   - Ensured that all endpoints, including `/test`, were accessible and functional.

3. **CI/CD Pipeline Integration**:
   - Configured GitLab CI/CD to automate the testing process.
   - Included steps in the CI pipeline to build the Docker images and run the unit tests.
   - Ensured that the pipeline only pushes the Docker image to Docker Hub if all tests pass.

4. **Manual Helm Chart Testing**:
   - Deployed the Helm charts manually due to the local Argo CD server not having a public IP.
   - Used `helm install` for the initial deployment and `helm upgrade` for subsequent updates to install and verify the deployment on the Kubernetes cluster.
   - Ensured all resources were correctly created and the application was functioning as expected.
   - Example commands used:
     ```bash
     helm install laravel ./helm
     helm upgrade laravel ./helm
     kubectl get pods
     kubectl get svc
     kubectl logs <pod-name>
     ```

### Issues Encountered and Resolutions

1. **Database Connection Issues**:
   - **Issue**: Encountered issues with the Laravel application connecting to the MySQL database.
   - **Resolution**: Ensured the correct database credentials were used and verified the Kubernetes service configurations.

2. **Environment Variable Configuration**:
   - **Issue**: Faced issues with environment variables not being correctly set in the CI/CD pipeline.
   - **Resolution**: Explicitly defined the necessary environment variables in the GitLab CI/CD settings and Kubernetes secrets.

3. **DNS Resolution in Kubernetes**:
   - **Issue**: Experienced issues with DNS resolution for the MySQL service within the Kubernetes cluster.
   - **Resolution**: Ensured the correct service names and namespace were used in the environment configurations.

## Troubleshooting

In light of the issues I have faced, below are some TS steps to solve any of them in case any arises.

### Database Connection Issues

**Issue**: The Laravel application is unable to connect to the MySQL database.

**Possible Causes and Resolutions**:
- **Incorrect Database Credentials**:
  - **Resolution**: Verify that the database credentials in the Kubernetes secrets and environment variables are correct. Ensure they match the credentials used in your `values.yaml` and `secret.yaml`.
    ```bash
    kubectl get secret laravel-secret -o yaml
    ```

- **MySQL Service Configuration**:
  - **Resolution**: Check that the MySQL service is running and accessible within the cluster. Ensure the service name and ports are correctly specified in the environment variables.
    ```bash
    kubectl get svc | grep mysql
    ```

- **Pod Logs**:
  - **Resolution**: Check the logs of the Laravel application pod for specific errors related to database connection.
    ```bash
    kubectl logs <laravel-pod-name>
    ```

### Environment Variable Issues

**Issue**: Environment variables are not being correctly set in the CI/CD pipeline or Kubernetes deployment.

**Possible Causes and Resolutions**:
- **Incorrect Environment Variable Configuration**:
  - **Resolution**: Ensure that all necessary environment variables are defined in the GitLab CI/CD settings and Kubernetes secrets.
  - Verify the deployment manifests reference the correct environment variables.
    ```bash
    kubectl describe deployment <deployment-name>
    ```

- **Secrets and ConfigMaps**:
  - **Resolution**: Ensure Kubernetes secrets and ConfigMaps are correctly created and mounted in the pods.
    ```bash
    kubectl get secret
    kubectl get configmap
    ```

### DNS Resolution Issues

**Issue**: The Laravel application is unable to resolve the MySQL service name within the Kubernetes cluster.

**Possible Causes and Resolutions**:
- **Incorrect Service Names**:
  - **Resolution**: Verify that the service names used in the environment configurations match the Kubernetes service names.
    ```bash
    kubectl get svc
    ```

### Helm Chart Deployment Issues

**Issue**: Helm charts were not deploying correctly due to missing configurations or incorrect values.

**Possible Causes and Resolutions**:
- **Invalid Configuration in `values.yaml`**:
  - **Resolution**: Validate the `values.yaml` file to ensure all necessary values are defined.
  - Use the `helm install` and `helm upgrade` commands to deploy the charts and verify the deployment status.
    ```bash
    helm install laravel ./helm
    helm upgrade laravel ./helm
    kubectl get pods
    kubectl get svc
    ```

- **Check Helm Release Status**:
  - **Resolution**: Check the Helm release status to identify any issues with the deployment.
    ```bash
    helm status <release-name>
    ```

### Access Denied for MySQL User

**Issue**: The Laravel application was unable to connect to MySQL due to access denied errors for the MySQL user.

**Possible Causes and Resolutions**:
- **Incorrect MySQL User Credentials**:
  - **Resolution**: Ensure that the MySQL user credentials are correctly set in the Kubernetes secrets and environment variables.
  - Verify that the MySQL user has the necessary permissions to access the specified database.
    ```bash
    kubectl get secret laravel-secret -o yaml
    ```

- **MySQL Logs**:
  - **Resolution**: Check the MySQL logs to identify and resolve any authentication issues.
    ```bash
    kubectl logs <mysql-pod-name>
    ```

### Persistent Volume Claims (PVC) Issues

**Issue**: PVCs were not being bound to the appropriate Persistent Volumes (PVs).

**Possible Causes and Resolutions**:
- **Mismatch in PV and PVC Definitions**:
  - **Resolution**: Ensure that the PV and PVC definitions match in terms of storage size, access modes, and storage class.
    ```bash
    kubectl get pv
    kubectl get pvc
    ```

- **Check PV and PVC Binding**:
  - **Resolution**: Verify that the PVs are available and correctly bound to the PVCs.
    ```bash
    kubectl describe pvc <pvc-name>
    ```
